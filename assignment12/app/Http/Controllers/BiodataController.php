<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BiodataController extends Controller
{
    public function daftar(){
        return view('page.daftar');
    }

    public function home(Request $request){
        $fullName = $request-> input('fname');
        $password = $request -> input('pass');
        $usia = $request -> input('usia');

        return view('page.home', ['fullName' => $fullName , 'password' => $password, 'usia' => $usia]);
    }
}
