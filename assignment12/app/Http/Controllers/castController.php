<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

class castController extends Controller
{
    public function create(){
        return view('cast.add');
    }

    public function store(Request $request){
        //validate
        $request->validate([
            'nama' => 'required|min:3',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        //insert to database
        DB::table('cast')->insert([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio')
        ]);

        //navigate to /cast
        return redirect('/cast');
    }

    public function index(){
        $cast = DB::table('cast')->get();

        return view('cast.tampil',['cast'=> $cast]);
    }

    public function show($id){
        $castData = DB::table('cast')->find($id);

        return view('cast.detail',['castData' => $castData]);
    }

    public function edit($id){
        $castData = DB::table('cast')->find($id);

        return view('cast.edit',['castData' => $castData]);
    }

    public function update($id, Request $request){
        //validate
        $request->validate([
            'nama' => 'required|min:3',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        DB::table('cast')
            ->where('id',$id)
            ->update(
                [
                'nama' => $request->input('nama'),
                'umur' => $request->input('umur'),
                'bio' => $request->input('bio')
                ]
                
            );
        return redirect('/cast');
    }

    public function destroy($id){
        DB::table('cast')->where('id','=', $id)->delete();

        return redirect('/cast');
    }
}
