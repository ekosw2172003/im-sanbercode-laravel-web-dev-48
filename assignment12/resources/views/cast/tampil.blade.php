@extends('layouts.master')

@section('judul')
Tampilan Casting
@endsection

@section('content')

<a href="/cast/create" class="btn btn-sm btn-primary my-3">Tambah Cast</a>

<table class="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Name</th>
        <th scope="col">Action</th>
        
      </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key => $item)
            <tr>
                <th scope="row">{{$key + 1 }}</th>
                <td>{{$item->nama}}</td>
                <td>
                    <form action="/cast/{{$item->id}}" method="post">
                        @csrf
                        @method('delete')
                        <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>  
            </tr>
        @empty
        <tr>
            <td>Tidak ada Data Cast</td>
        </tr>
            
        @endforelse
      
    </tbody>
  </table>
@endsection
