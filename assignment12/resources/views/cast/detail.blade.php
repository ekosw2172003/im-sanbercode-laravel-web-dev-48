@extends('layouts.master')

@section('judul')
Detail Kategori
@endsection

@section('content')
<h1 class="text-primary"> {{$castData->nama}}</h1>
<p>Umur : {{$castData->umur}}</p>
<p>Bio : {{$castData->bio}}</p>
@endsection