<?php

use App\Http\Controllers\BiodataController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\castController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[DashboardController::class, 'utama']);
Route::get('/daftar',[BiodataController::class, 'daftar']);

Route::post('/home', [BiodataController::class ,'home']);

Route::get('/table',function(){
    return view('page.table');
});

Route::get('/data-table',function(){
    return view('page.data-table');
});

//CRUD CAST

Route::get('/cast/create',[castController::class, 'create']);

Route::post('/cast',[castController::class, 'store']);

Route::get('/cast',[castController::class, 'index']);

Route::get('/cast/{id}',[castController::class, 'show']);

Route::get('/cast/{id}/edit',[castController::class, 'edit']);

Route::put('/cast/{id}',[castController::class, 'update']);

Route::delete('/cast/{id}',[castController::class, 'destroy']);